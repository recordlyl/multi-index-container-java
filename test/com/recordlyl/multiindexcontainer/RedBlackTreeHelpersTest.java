package com.recordlyl.multiindexcontainer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import com.google.common.collect.Ordering;

public class RedBlackTreeHelpersTest
{
    private Collection<List<Integer>> VALUE_SETS = null;

    @Before
    public void generateValues()
    {
        List<Integer> values = new ArrayList<Integer>(6);
        values.add(1);
        values.add(2);
        values.add(3);
        values.add(4);
        values.add(5);
        values.add(6);

        VALUE_SETS = Collections2.permutations(values);
    }

    @After
    public void dropValues()
    {
        VALUE_SETS = null;
    }

    @Test
    public void testInsertDelete()
    {
        for (List<Integer> values : VALUE_SETS)
        {
            RedBlackTreeHelpers<Integer, Integer> helper = createHelper();

            for (int value : values)
                assertNull(helper.find(value));

            for (int i = 0; i < values.size(); ++i)
            {
                System.out.println("stepInsert(" + values.get(i) + "): " + Joiner.on(", ").join(values));

                assertTrue(validateTreeStructure(helper, i));
                assertTrue(validateRBProperties(helper));

                Node<Integer> node = new Node<Integer>(values.get(i), 5);

                assertTrue(helper.insert(node));
                assertTrue(validateTreeStructure(helper, i + 1));

                helper.insertCancel(node);
                assertTrue(validateTreeStructure(helper, i));
                assertTrue(validateRBProperties(helper));

                for (int j = 0; j < i; ++j)
                {
                    Integer       valueToFind = values.get(j);
                    Node<Integer> nodeFound   = helper.find(valueToFind);

                    assertNotNull(nodeFound);
                    assertEquals(valueToFind, nodeFound.value);
                }

                for (int j = i; j < values.size(); ++j)
                    assertNull(helper.find(values.get(j)));

                assertTrue(helper.insert(node));
                assertTrue(validateTreeStructure(helper, i + 1));

                helper.insertFixUp(node);

                assertTrue(validateTreeStructure(helper, i + 1));
                assertTrue(validateRBProperties(helper));

                for (List<Integer> deletionValues : Collections2.permutations(values.subList(0, helper.getCount())))
                {
                    RedBlackTreeHelpers<Integer, Integer> helperForDeletion = duplicateHelper(helper);

                    assertTrue(validateTreeStructure(helperForDeletion, helper.getCount()));
                    assertTrue(validateRBProperties(helperForDeletion));

                    for (int j = 0; j < deletionValues.size(); ++j)
                    {
                        int           valueForDeletion = deletionValues.get(j);
                        Node<Integer>  nodeForDeletion = helperForDeletion.find(valueForDeletion);
                        assertNotNull(nodeForDeletion);
                        assertEquals(valueForDeletion, (int) nodeForDeletion.value);

                        helperForDeletion.delete(nodeForDeletion);

                        assertTrue(validateTreeStructure(helperForDeletion, helper.getCount() - 1 - j));
                        assertTrue(validateRBProperties(helperForDeletion));
                    }
                }

                for (int valueNotInserted : values.subList(helper.getCount(), values.size()))
                    assertNull(helper.find(valueNotInserted));
            }
        }
    }

    @Test
    public void testBounds()
    {
        {
            RedBlackTreeHelpers<Integer, Integer> helper = createHelper();

            assertNull(helper.getLowerBound( 0)); assertNull(helper.getUpperBound( 0));
            assertNull(helper.getLowerBound( 1)); assertNull(helper.getUpperBound( 1));
            assertNull(helper.getLowerBound( 2)); assertNull(helper.getUpperBound( 2));
            assertNull(helper.getLowerBound( 3)); assertNull(helper.getUpperBound( 3));
            assertNull(helper.getLowerBound( 4)); assertNull(helper.getUpperBound( 4));
            assertNull(helper.getLowerBound( 5)); assertNull(helper.getUpperBound( 5));
            assertNull(helper.getLowerBound( 6)); assertNull(helper.getUpperBound( 6));
            assertNull(helper.getLowerBound( 7)); assertNull(helper.getUpperBound( 7));
            assertNull(helper.getLowerBound( 8)); assertNull(helper.getUpperBound( 8));
            assertNull(helper.getLowerBound( 9)); assertNull(helper.getUpperBound( 9));
            assertNull(helper.getLowerBound(10)); assertNull(helper.getUpperBound(10));

            for (int i = 0; i <= 10; ++i)
            {
                for (int j = 0; j < 10; ++j)
                {
                    if (i >= j)
                    {
                        try
                        {
                            helper.getRange(i, j);
                            fail("did not throw exception for invalid range");
                        }
                        catch (IllegalArgumentException e)
                        {
                        }
                    }
                    else
                    {
                        assertNull(helper.getRange(i, j));
                    }
                }
            }
        }

        List<Integer> possibleValues = new ArrayList<Integer>(5);
        possibleValues.add(1);
        possibleValues.add(3);
        possibleValues.add(5);
        possibleValues.add(7);
        possibleValues.add(9);

        for (List<Integer> values : Collections2.permutations(possibleValues))
        {
            RedBlackTreeHelpers<Integer, Integer> helper = createHelper();

            for (int value : values)
            {
                Node<Integer> node = new Node<Integer>(value, 5);
                assertTrue(helper.insert(node));
                helper.insertFixUp(node);
            }

            for (int i = 0; i <= 10; ++i)
            {
                for (int j = 0; j <= 10; ++j)
                {
                    if (i >= j)
                    {
                        try
                        {
                            helper.getRange(i, j);
                            fail("did not throw exception for invalid range");
                        }
                        catch (IllegalArgumentException e)
                        {
                        }
                    }
                    else
                    {
                        int count = 0;
                        for (int value : values)
                            if (i <= value && value < j)
                                ++count;

                        if (count == 0)
                        {
                            assertNull(helper.getRange(i, j));
                        }
                        else
                        {
                            Pair<Node<Integer>, Node<Integer>> range = helper.getRange(i, j);
                            assertNotNull(range);

                            Node<Integer> first     = range.first;
                            Node<Integer> last      = range.second;
                            int           nodeCount = 1;

                            for (; first != last; first = helper.getSuccessor(first))
                                ++nodeCount;

                            assertEquals(count, nodeCount);
                        }
                    }
                }
            }

            {
                Node<Integer> l = helper.getLowerBound(0);
                Node<Integer> u = helper.getUpperBound(0);

                assertNotNull(l); assertEquals(1, (int) l.value);
                assertNotNull(u); assertEquals(1, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(1);
                Node<Integer> u = helper.getUpperBound(1);

                assertNotNull(l); assertEquals(1, (int) l.value);
                assertNotNull(u); assertEquals(3, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(2);
                Node<Integer> u = helper.getUpperBound(2);

                assertNotNull(l); assertEquals(3, (int) l.value);
                assertNotNull(u); assertEquals(3, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(3);
                Node<Integer> u = helper.getUpperBound(3);

                assertNotNull(l); assertEquals(3, (int) l.value);
                assertNotNull(u); assertEquals(5, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(4);
                Node<Integer> u = helper.getUpperBound(4);

                assertNotNull(l); assertEquals(5, (int) l.value);
                assertNotNull(u); assertEquals(5, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(5);
                Node<Integer> u = helper.getUpperBound(5);

                assertNotNull(l); assertEquals(5, (int) l.value);
                assertNotNull(u); assertEquals(7, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(6);
                Node<Integer> u = helper.getUpperBound(6);

                assertNotNull(l); assertEquals(7, (int) l.value);
                assertNotNull(u); assertEquals(7, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(7);
                Node<Integer> u = helper.getUpperBound(7);

                assertNotNull(l); assertEquals(7, (int) l.value);
                assertNotNull(u); assertEquals(9, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(8);
                Node<Integer> u = helper.getUpperBound(8);

                assertNotNull(l); assertEquals(9, (int) l.value);
                assertNotNull(u); assertEquals(9, (int) u.value);
            }

            {
                Node<Integer> l = helper.getLowerBound(9);
                Node<Integer> u = helper.getUpperBound(9);

                assertNotNull(l); assertEquals(9, (int) l.value);
                assertNull(u);
            }

            {
                Node<Integer> l = helper.getLowerBound(10);
                Node<Integer> u = helper.getUpperBound(10);

                assertNull(l);
                assertNull(u);
            }
        }
    }

    private RedBlackTreeHelpers<Integer, Integer> createHelper()
    {
        Function<Integer, Integer> conv = Functions.identity();
        Comparator<Integer>        comp = Ordering.natural();

        RedBlackTreeHelpers<Integer, Integer> helper = new RedBlackTreeHelpers<Integer, Integer>(conv, comp);

        helper.setIdxColor(0);
        helper.setIdxKey(1);
        helper.setIdxParent(2);
        helper.setIdxLeft(3);
        helper.setIdxRight(4);

        return helper;
    }

    private RedBlackTreeHelpers<Integer, Integer> duplicateHelper(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        RedBlackTreeHelpers<Integer, Integer> newHelper = new RedBlackTreeHelpers<Integer, Integer>(helper.getConv(), helper.getComp());

        newHelper.setIdxColor(helper.getIdxColor());
        newHelper.setIdxKey(helper.getIdxKey());
        newHelper.setIdxParent(helper.getIdxParent());
        newHelper.setIdxLeft(helper.getIdxLeft());
        newHelper.setIdxRight(helper.getIdxRight());

        newHelper.setRoot(duplicateNode(helper.getRoot(), helper, newHelper));
        newHelper.setCount(helper.getCount());

        return newHelper;
    }

    private Node<Integer> duplicateNode(Node<Integer> node, RedBlackTreeHelpers<Integer, Integer> helper, RedBlackTreeHelpers<Integer, Integer> newHelper)
    {
        if (node == null)
            return null;

        Node<Integer> newLeft  = duplicateNode(helper.getLeft(node), helper, newHelper);
        Node<Integer> newRight = duplicateNode(helper.getRight(node), helper, newHelper);

        Node<Integer> newNode = new Node<Integer>(node.value, 5);
        newHelper.setColor(newNode, helper.getColor(node));
        newHelper.setKey(newNode, helper.getKey(node));

        newHelper.setParent(newNode, null);
        newHelper.setLeft(newNode, newLeft);
        newHelper.setRight(newNode, newRight);

        if (newLeft != null)
            newHelper.setParent(newLeft, newNode);

        if (newRight != null)
            newHelper.setParent(newRight, newNode);

        return newNode;
    }

    private boolean validateRBProperties(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        boolean p1 = isRootBlack(helper);
        boolean p2 = isChildrenOfRedNodeAllBlack(helper);
        boolean p3 = getBlackNodeCountInPath(helper) != -1;

        if (p1 && p2 && p3)
            return true;

        String msg = "properties is broken:";
        if (!p1)
            msg += "\n (a) root is not black";
        if (!p2)
            msg += "\n (b) children of red nodes aren't all black";
        if (!p3)
            msg += "\n (c) black nodes count is not the same in every path";
        System.out.println(msg);
        dumpNodes(helper);

        return false;
    }

    private boolean isRootBlack(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        Node<Integer> root = helper.getRoot();

        if (root == null)
            return true;

        return helper.isBlack(root);
    }

    private boolean isChildrenOfRedNodeAllBlack(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        return isChildrenOfRedNodeAllBlack(helper, helper.getRoot());
    }

    private boolean isChildrenOfRedNodeAllBlack(RedBlackTreeHelpers<Integer, Integer> helper, Node<Integer> node)
    {
        if (node == null)
            return true;

        Node<Integer> left  = helper.getLeft(node);
        Node<Integer> right = helper.getRight(node);

        if (helper.isRed(node))
        {
            if (left != null)
                if (!helper.isBlack(left))
                    return false;

            if (right != null)
                if (!helper.isBlack(right))
                    return false;
        }

        if (!isChildrenOfRedNodeAllBlack(helper, left))
            return false;

        if (!isChildrenOfRedNodeAllBlack(helper, right))
            return false;

        return true;
    }

    private int getBlackNodeCountInPath(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        return getBlackNodeCountInPath(helper, helper.getRoot());
    }

    private int getBlackNodeCountInPath(RedBlackTreeHelpers<Integer, Integer> helper, Node<Integer> node)
    {
        if (node == null)
            return 0;

        Node<Integer> left  = helper.getLeft(node);
        Node<Integer> right = helper.getRight(node);

        int  leftCount = getBlackNodeCountInPath(helper, left);
        int rightCount = getBlackNodeCountInPath(helper, right);

        if (leftCount == -1)
            return -1;

        if (rightCount == -1)
            return -1;

        if (leftCount != rightCount)
            return -1;

        if (helper.isBlack(node))
            return leftCount + 1;
        else
            return leftCount;
    }

    private boolean validateTreeStructure(RedBlackTreeHelpers<Integer, Integer> helper, int nodeCount)
    {
        int nodeCount1 = countNodes(helper);
        int nodeCount2 = helper.getCount();

        boolean isCount1Correct    = nodeCount == nodeCount1;
        boolean isCount2Correct    = nodeCount == nodeCount2;
        boolean isLinksCorrect     = validateNodeLinks(helper);
        boolean isBinarySearchTree = validateMinMax(helper); // TODO test if still a binary search tree

        if (isCount1Correct && isCount2Correct && isLinksCorrect && isBinarySearchTree)
            return true;

        String msg = "tree structure ill-formed:";
        if (!isCount1Correct)
            msg += "\n (a) node count mismatch from \"countNodes\" and nodeCount: " + nodeCount1 + " != " + nodeCount;
        if (!isCount2Correct)
            msg += "\n (b) node count mismatch from \"getCount\" and nodeCount: " + nodeCount2 + " != " + nodeCount;
        if (!isLinksCorrect)
            msg += "\n (c) links between nodes is broken";
        if (!isBinarySearchTree)
            msg += "\n (d) invalid binary search tree";
        System.out.println(msg);
        dumpNodes(helper);

        return false;
    }

    private int countNodes(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        return countNodes(helper, helper.getRoot());
    }

    private int countNodes(RedBlackTreeHelpers<Integer, Integer> helper, Node<Integer> node)
    {
        if (node == null)
            return 0;

        Node<Integer> left  = helper.getLeft(node);
        Node<Integer> right = helper.getRight(node);

        return 1 + countNodes(helper, left) + countNodes(helper, right);
    }

    private boolean validateMinMax(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        Node<Integer> root = helper.getRoot();
        if (root == null)
            return true;

        return getMinMax(helper, root) != null;
    }

    private int[] getMinMax(RedBlackTreeHelpers<Integer, Integer> helper, Node<Integer> node)
    {
        assert node != null;

        Node<Integer> left  = helper.getLeft(node);
        Node<Integer> right = helper.getRight(node);

        int min = node.value;
        int max = node.value;

        if (left != null)
        {
            int[] leftMinmax = getMinMax(helper, left);
            if (leftMinmax == null)
                return null;

            // should be:
            //   leftMin <= leftMax < min <= max
            int leftMin = leftMinmax[0];
            int leftMax = leftMinmax[1];

            if (leftMax >= min)
                return null;

            min = leftMin;
        }

        if (right != null)
        {
            int[] rightMinmax = getMinMax(helper, right);
            if (rightMinmax == null)
                return null;

            // should be:
            //   min <= max < rightMin <= rightMax
            int rightMin = rightMinmax[0];
            int rightMax = rightMinmax[1];

            if (max >= rightMin)
                return null;

            max = rightMax;
        }

        return new int[] {min, max};
    }

    private boolean validateNodeLinks(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        return validateNodeLinks(helper, helper.getRoot());
    }

    private boolean validateNodeLinks(RedBlackTreeHelpers<Integer, Integer> helper, Node<Integer> node)
    {
        if (node == null)
            return true;

        {
            Node<Integer> left = helper.getLeft(node);

            if (left != null)
                if (helper.getParent(left) != node)
                    return false;

            if (!validateNodeLinks(helper, left))
                return false;
        }

        {
            Node<Integer> right = helper.getRight(node);

            if (right != null)
                if (helper.getParent(right) != node)
                    return false;

            if (!validateNodeLinks(helper, right))
                return false;
        }

        return true;
    }

    private void dumpNodes(RedBlackTreeHelpers<Integer, Integer> helper)
    {
        System.out.println("=dump nodes:");

        dumpNodes(helper, helper.getRoot());
    }

    private void dumpNodes(RedBlackTreeHelpers<Integer, Integer> helper, Node<Integer> node)
    {
        if (node == null)
            return;

        Node<Integer> parent = helper.getParent(node);
        Node<Integer> left   = helper.getLeft(node);
        Node<Integer> right  = helper.getRight(node);

        String  color       = helper.isRed(node) ? "(red)" : "(black)";
        Integer parentValue = parent == null ? null : parent.value;
        Integer leftValue   = left   == null ? null :   left.value;
        Integer rightValue  = right  == null ? null :  right.value;

        System.out.println(" " + node.value + color + ": " + parentValue + " " + leftValue + " " + rightValue);

        dumpNodes(helper, left);
        dumpNodes(helper, right);
    }
}
