package com.recordlyl.multiindexcontainer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterators;

public class MultiIndexContainerTest
{
    private Collection<List<Integer>> VALUE_SETS = null;

    @Before
    public void generateValues()
    {
        List<Integer> values = new ArrayList<Integer>(6);
        values.add(1);
        values.add(2);
        values.add(3);
        values.add(4);
        values.add(5);
        values.add(6);

        VALUE_SETS = Collections2.permutations(values);
    }

    @After
    public void dropValues()
    {
        VALUE_SETS = null;
    }

    @Test
    public void testEmpty()
    {
        Pair<MultiIndexContainer<Integer>, SortedMapView<Integer, Integer>> pair = createContainer();

        MultiIndexContainer<Integer>    container = pair.first;
        SortedMapView<Integer, Integer> map       = pair.second;

        assertEquals(0, container.size());
        assertEquals(0, map      .size());

        Iterator<Integer>                     i1 = container.iterator();
        Iterator<Map.Entry<Integer, Integer>> i2 = map      .iterator();
        Iterator<Integer>                     i3 = map      .keyIterator();
        Iterator<Integer>                     i4 = map      .valueIterator();

        assertNotNull(i1); assertFalse(i1.hasNext());
        assertNotNull(i2); assertFalse(i2.hasNext());
        assertNotNull(i3); assertFalse(i3.hasNext());
        assertNotNull(i4); assertFalse(i4.hasNext());
    }

    @Test
    public void testAll()
    {
        for (List<Integer> values : VALUE_SETS)
        {
            System.out.println("testAll: " + Joiner.on(", ").join(values));

            assertFalse(values.isEmpty());

            for (int i = 1; i < values.size(); ++i)
            {
                Pair<MultiIndexContainer<Integer>, SortedMapView<Integer, Integer>> pair = createContainer();

                MultiIndexContainer<Integer>    container      = pair.first;
                SortedMapView<Integer, Integer> map            = pair.second;
                List<Integer>                   insertedValues = new ArrayList<Integer>(values.subList(0, i));

                for (int insertedValue : insertedValues)
                    assertTrue(container.add(insertedValue));

                assertEquals(i, container.size());
                assertEquals(i, map      .size());

                assertTrue(Iterators.elementsEqual(insertedValues.iterator(), container.iterator()));

                Collections.sort(insertedValues);
                assertTrue(Iterators.elementsEqual(insertedValues.iterator(), map.keyIterator()));
                assertTrue(Iterators.elementsEqual(insertedValues.iterator(), map.valueIterator()));

                Iterator<Map.Entry<Integer, Integer>> entryIterator = Iterators.transform(
                    insertedValues.iterator(),
                    new Function<Integer, Map.Entry<Integer, Integer>>()
                    {
                        @Override
                        public Entry<Integer, Integer> apply(Integer input)
                        {
                            return new SimpleEntry<Integer>(input);
                        }
                    }
                );
                assertTrue(Iterators.elementsEqual(entryIterator, map.iterator()));
            }
        }
    }

    private Pair<MultiIndexContainer<Integer>, SortedMapView<Integer, Integer>> createContainer()
    {
        MultiIndexContainer<Integer>    container = new MultiIndexContainer<Integer>();
        SortedMapView<Integer, Integer> map       = container.sortedMapView(Functions.<Integer>identity());

        return new Pair<MultiIndexContainer<Integer>, SortedMapView<Integer,Integer>>(container, map);
    }

    private static class SimpleEntry<E> implements Map.Entry<Integer, Integer>
    {
        private Integer i;

        public SimpleEntry(Integer i)
        {
            this.i = i;
        }

        @Override
        public Integer getKey()
        {
            return i;
        }

        @Override
        public Integer getValue()
        {
            return i;
        }

        @Override
        public Integer setValue(Integer paramV)
        {
            throw new UnsupportedOperationException("testing should not modify entry through this iterator");
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;

            if (obj instanceof Map.Entry)
            {
                @SuppressWarnings("unchecked")
                Map.Entry<E, E> entry = (Entry<E, E>) obj;

                boolean isKeySame = i == null ? entry.getKey() == null : i.equals(entry.getKey());
                if (!isKeySame)
                    return false;

                boolean isValueSame = i == null ? entry.getValue() == null : i.equals(entry.getValue());
                if (!isValueSame)
                    return false;

                return true;
            }

            return false;
        }

        @Override
        public int hashCode()
        {
            return Objects.hashCode(i, i);
        }

    }
}
