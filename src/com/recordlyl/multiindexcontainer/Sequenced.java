package com.recordlyl.multiindexcontainer;

import java.util.Iterator;

class Sequenced<E> implements SequencedView<E>, SubView<E>
{
    private MainView<E> mainView;
    private int         payloadIndex;
    private Node<E>     head;
    private Node<E>     tail;
    private int         count;

    Sequenced(MainView<E> mainView, int payloadIndex)
    {
        assert payloadIndex >= 0;

        this.mainView     = mainView;
        this.payloadIndex = payloadIndex;
        this.head         = null;
        this.tail         = null;
        this.count        = 0;
    }

    @Override
    public int getPayloadCount()
    {
        // (prev, next)
        return 2;
    }

    @Override
    public int getPayloadIndex()
    {
        return payloadIndex;
    }

    @Override
    public int size()
    {
        return count;
    }

    @Override
    public boolean add(E e)
    {
        return mainView.add(e);
    }

    @Override
    public boolean remove(E e)
    {
        for (Node<E> node = head; node != null; node = getNext(node))
        {
            boolean equal = (node.value == null && e == null                        ) ||
                            (node.value != null && e != null && node.value.equals(e))
                            ;

            if (equal)
                return mainView.remove(node);
        }

        return false;
    }

    @Override
    public Iterator<E> iterator()
    {
        return new Iterator<E>()
        {
            private Node<E> pos = head;

            @Override
            public boolean hasNext()
            {
                return pos != null;
            }

            @Override
            public E next()
            {
                E value = pos.value;

                pos = getNext(pos);

                return value;
            }

            @Override
            public void remove()
            {
                throw new UnsupportedOperationException("element removal is not supported by this iterator");
            }
        };
    }

    @Override
    public boolean reserveAddNode(Node<E> node)
    {
        setNext(node, null);

        if (tail == null)
        {
            setPrev(node, null);
            setNext(node, null);

            head = node;
            tail = node;
        }
        else
        {
            setPrev(node, tail);
            setNext(tail, node);

            tail = node;
        }

        ++count;

        return true;
    }

    @Override
    public void commitAddNode(Node<E> node)
    {
        // nothing to do
    }

    @Override
    public void revertAddNode(Node<E> node)
    {
        assert node == tail;

        if (head == tail)
        {
            head = null;
            tail = null;
        }
        else
        {
            Node<E> originTail = getPrev(node);

            setNext(originTail, null);

            tail = originTail;
        }

        setPrev(node, null);
        setNext(node, null);

        --count;
    }

    @Override
    public void removeNode(Node<E> node)
    {
        assert head != null;
        assert tail != null;

        Node<E> prev = getPrev(node);
        Node<E> next = getNext(node);

        setPrev(node, null);
        setNext(node, null);

        // 1. (prev, next) == (non-null, non-null) => in the middle, create links between prev and next
        // 2. (prev, next) == (non-null,     null) => tail node, set next node of prev to null and prev as new tail
        // 3. (prev, next) == (    null, non-null) => head node, set prev node of next to null and next as new head
        // 4. (prev, next) == (    null,     null) => the only one node, set head and tail to null

        if (prev != null)
            setNext(prev, next);
        else
            head = next;

        if (next != null)
            setPrev(next, prev);
        else
            tail = prev;

        --count;
    }

    @SuppressWarnings("unchecked")
    private Node<E> getPrev(Node<E> node)
    {
        return (Node<E>) node.payloads[getPayloadIndex()];
    }

    private void setPrev(Node<E> node, Node<E> prev)
    {
        node.payloads[getPayloadIndex()] = prev;
    }

    @SuppressWarnings("unchecked")
    private Node<E> getNext(Node<E> node)
    {
        return (Node<E>) node.payloads[getPayloadIndex() + 1];
    }

    private void setNext(Node<E> node, Node<E> next)
    {
        node.payloads[getPayloadIndex() + 1] = next;
    }
}
