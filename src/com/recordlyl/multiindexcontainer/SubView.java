package com.recordlyl.multiindexcontainer;

interface SubView<E>
{
    int getPayloadCount();
    int getPayloadIndex();

    int size();

    boolean reserveAddNode(Node<E> node);
    void     commitAddNode(Node<E> node);
    void     revertAddNode(Node<E> node);

    void removeNode(Node<E> node); // MUST not throw
}
