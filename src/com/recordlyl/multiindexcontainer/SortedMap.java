package com.recordlyl.multiindexcontainer;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Function;
import com.google.common.base.Objects;

class SortedMap<K, V> implements SortedMapView<K, V>, SubView<V>
{
    private MainView<V>               mainView;
    private int                       payloadIndex;
    private RedBlackTreeHelpers<K, V> helper;

    SortedMap(MainView<V> mainView, Function<V, K> conv, Comparator<K> comp, int payloadIndex)
    {
        assert payloadIndex >= 0;

        this.mainView     = mainView;
        this.payloadIndex = payloadIndex;
        this.helper       = new RedBlackTreeHelpers<K, V>(conv, comp);

        this.helper.setIdxKey(this.payloadIndex);
        this.helper.setIdxColor(this.payloadIndex + 1);
        this.helper.setIdxParent(this.payloadIndex + 2);
        this.helper.setIdxLeft(this.payloadIndex + 3);
        this.helper.setIdxRight(this.payloadIndex + 4);
    }

    @Override
    public int getPayloadCount()
    {
        // (key, up, left, right, color)
        return 5;
    }

    @Override
    public int getPayloadIndex()
    {
        return payloadIndex;
    }

    @Override
    public int size()
    {
        return helper.getCount();
    }

    @Override
    public boolean add(V e)
    {
        return mainView.add(e);
    }

    @Override
    public boolean remove(K key)
    {
        Node<V> node = helper.find(key);

        if (node == null)
            return false;
        else
            return mainView.remove(node);
    }

    @Override
    public boolean remove(K begin, K end)
    {
        Pair<Node<V>, Node<V>> range = helper.getRange(begin, end);
        if (range == null)
            return false;

        Node<V> firstNode = range.first;
        Node<V>  lastNode = range.second;

        while (firstNode != lastNode)
        {
            Node<V> nextFirstNode = helper.getPredecessor(firstNode);

            mainView.remove(firstNode);

            firstNode = nextFirstNode;
        }

        mainView.remove(lastNode);

        return true;
    }

    @Override
    public Iterator<Map.Entry<K, V>> iterator()
    {
        return new SimpleIterator<K, V, Map.Entry<K, V>>(helper)
        {
            @Override
            public Map.Entry<K, V> next()
            {
                K key   = helper.getKey(pos);
                V value = pos.value;

                moveToNext(helper);

                return new SimpleEntry<K, V>(key, value);
            }
        };
    }

    @Override
    public Iterator<K> keyIterator()
    {
        return new SimpleIterator<K, V, K>(helper)
        {
            @Override
            public K next()
            {
                K key = helper.getKey(pos);

                moveToNext(helper);

                return key;
            }
        };
    }

    @Override
    public Iterator<V> valueIterator()
    {
        return new SimpleIterator<K, V, V>(helper)
        {
            @Override
            public V next()
            {
                V value = pos.value;

                moveToNext(helper);

                return value;
            }
        };
    }

    @Override
    public boolean reserveAddNode(Node<V> node)
    {
        return helper.insert(node);
    }

    @Override
    public void commitAddNode(Node<V> node)
    {
        helper.insertFixUp(node);
    }

    @Override
    public void revertAddNode(Node<V> node)
    {
        helper.insertCancel(node);
    }

    @Override
    public void removeNode(Node<V> node)
    {
        helper.delete(node);
    }

    private static class SimpleEntry<K, V> implements Map.Entry<K, V>
    {
        private K key;
        private V value;

        public SimpleEntry(K key, V value)
        {
            this.key   = key;
            this.value = value;
        }

        @Override
        public K getKey()
        {
            return key;
        }

        @Override
        public V getValue()
        {
            return value;
        }

        @Override
        public V setValue(V paramV)
        {
            throw new UnsupportedOperationException("element updating is not supported by this iterator");
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;

            if (obj instanceof Map.Entry)
            {
                @SuppressWarnings("unchecked")
                Map.Entry<K, V> entry = (Entry<K, V>) obj;

                boolean isKeySame = key == null ? entry.getKey() == null : key.equals(entry.getKey());
                if (!isKeySame)
                    return false;

                boolean isValueSame = value == null ? entry.getValue() == null : value.equals(entry.getValue());
                if (!isValueSame)
                    return false;

                return true;
            }

            return false;
        }

        @Override
        public int hashCode()
        {
            return Objects.hashCode(key, value);
        }

        @Override
        public String toString()
        {
            return key + "=" + value;
        }

    };

    private abstract static class SimpleIterator<K, V, E> implements Iterator<E>
    {
        protected Node<V> pos;

        public SimpleIterator(RedBlackTreeHelpers<K, V> helper)
        {
            pos = helper.getRoot();

            if (pos != null)
                pos = helper.getMinNode(pos);
        }

        @Override
        public boolean hasNext()
        {
            return pos != null;
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException("element removal is not supported by this iterator");
        }

        protected void moveToNext(RedBlackTreeHelpers<K, V> helper)
        {
            pos = helper.getSuccessor(pos);
        }
    }
}
