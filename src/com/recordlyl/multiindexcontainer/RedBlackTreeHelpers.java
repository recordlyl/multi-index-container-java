package com.recordlyl.multiindexcontainer;

import java.util.Comparator;

import com.google.common.base.Function;

class RedBlackTreeHelpers<K, V>
{
    private static final Integer RED   = 0;
    private static final Integer BLACK = 1;

    private int idxKey   ;
    private int idxColor ;
    private int idxParent;
    private int idxLeft  ;
    private int idxRight ;

    private Function<V, K> conv ;
    private Comparator<K>  comp ;
    private Node<V>        root ;
    private int            count;

    public RedBlackTreeHelpers(Function<V, K> conv, Comparator<K> comp)
    {
        this.idxKey    = -1;
        this.idxColor  = -1;
        this.idxParent = -1;
        this.idxLeft   = -1;
        this.idxRight  = -1;

        this.conv  = conv;
        this.comp  = comp;
        this.root  = null;
        this.count = 0;
    }

    public int getIdxKey()
    {
        return idxKey;
    }

    public void setIdxKey(int idxKey)
    {
        this.idxKey = idxKey;
    }

    public int getIdxColor()
    {
        return idxColor;
    }

    public void setIdxColor(int idxColor)
    {
        this.idxColor = idxColor;
    }

    public int getIdxParent()
    {
        return idxParent;
    }

    public void setIdxParent(int idxParent)
    {
        this.idxParent = idxParent;
    }

    public int getIdxLeft()
    {
        return idxLeft;
    }

    public void setIdxLeft(int idxLeft)
    {
        this.idxLeft = idxLeft;
    }

    public int getIdxRight()
    {
        return idxRight;
    }

    public void setIdxRight(int idxRight)
    {
        this.idxRight = idxRight;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public Node<V> find(K key)
    {
        for (Node<V> node = root; node != null; )
        {
            K   nodeKey = getKey(node);
            int cmp     = comp.compare(key, nodeKey);

            if (cmp == 0)
                return node;

            node = cmp < 0 ? getLeft(node) : getRight(node);
        }

        return null;
    }

    public Node<V> getLowerBound(K key)
    {
        if (root == null)
            return null;

        for (Node<V> node = root; ; )
        {
            assert node != null;

            K   nodeKey = getKey(node);
            int cmp     = comp.compare(key, nodeKey);

            if (cmp == 0)
            {
                // for example:
                //   search (a) ~ (e) in the following tree
                //     (a) 1 on 1
                //     (b) 3 on 3
                //     (c) 5 on 5
                //     (d) 7 on 7
                //     (e) 9 on 9
                //
                //        5
                //       / \
                //      /   \
                //     1     9
                //      \   /
                //      3   7

                // (a) => 1
                // (b) => 3
                // (c) => 5
                // (d) => 7
                // (e) => 9
                return node;
            }
            else if (cmp > 0)
            {
                // for example:
                //   search (a) ~ (e) in the following tree
                //     (a)  2 on 1
                //     (b)  4 on 3
                //     (c)  6 on 5
                //     (d)  8 on 7
                //     (e) 10 on 9
                //
                //        5
                //       / \
                //      /   \
                //     1     9
                //      \   /
                //      3   7
                Node<V> right = getRight(node);

                // (b) => 5
                // (d) => 9
                // (e) => null
                // case (a) and (c)
                if (right == null)
                    return getSuccessor(node);

                // (a) => 3
                // (c) => 9
                node = right;
            }
            else
            {
                // for example:
                //   search (a) ~ (e) in the following tree
                //     (a) 0 on 1
                //     (b) 2 on 3
                //     (c) 4 on 5
                //     (d) 6 on 7
                //     (e) 8 on 9
                //
                //        5
                //       / \
                //      /   \
                //     1     9
                //      \   /
                //      3   7
                Node<V> left = getLeft(node);

                // (a) => 1
                // (b) => 3
                // (d) => 7
                if (left == null)
                    return node;

                // (c) => 1
                // (e) => 7
                node = left;
            }
        }
    }

    public Node<V> getUpperBound(K key)
    {
        if (root == null)
            return null;

        for (Node<V> node = root; ; )
        {
            assert node != null;

            K   nodeKey = getKey(node);
            int cmp     = comp.compare(key, nodeKey);

            if (cmp == 0)
            {
                // for example:
                //   search (a) ~ (e) in the following tree
                //     (a) 1 on 1
                //     (b) 3 on 3
                //     (c) 5 on 5
                //     (d) 7 on 7
                //     (e) 9 on 9
                //
                //        5
                //       / \
                //      /   \
                //     1     9
                //      \   /
                //      3   7

                // (a) => 3
                // (b) => 5
                // (c) => 7
                // (d) => 9
                // (e) => null
                return getSuccessor(node);
            }
            else if (cmp > 0)
            {
                // for example:
                //   search (a) ~ (e) in the following tree
                //     (a)  2 on 1
                //     (b)  4 on 3
                //     (c)  6 on 5
                //     (d)  8 on 7
                //     (e) 10 on 9
                //
                //        5
                //       / \
                //      /   \
                //     1     9
                //      \   /
                //      3   7
                Node<V> right = getRight(node);

                // (b) => 5
                // (d) => 9
                // (e) => null
                // case (a) and (c)
                if (right == null)
                    return getSuccessor(node);

                // (a) => 3
                // (c) => 9
                node = right;
            }
            else
            {
                // for example:
                //   search (a) ~ (e) in the following tree
                //     (a) 0 on 1
                //     (b) 2 on 3
                //     (c) 4 on 5
                //     (d) 6 on 7
                //     (e) 8 on 9
                //
                //        5
                //       / \
                //      /   \
                //     1     9
                //      \   /
                //      3   7
                Node<V> left = getLeft(node);

                // (a) => 1
                // (b) => 3
                // (d) => 7
                if (left == null)
                    return node;

                // (c) => 1
                // (e) => 7
                node = left;
            }
        }
    }

    public Pair<Node<V>, Node<V>> getRange(K begin, K end)
    {
        if (comp.compare(begin, end) >= 0)
            throw new IllegalArgumentException("[" + begin + ", " + end + ") is not valid half-open range");

        if (root == null)
            return null;

        Node<V> beginNode = getLowerBound(begin);
        if (beginNode == null)
            return null;

        Node<V> endNode = getLowerBound(end);

        // 1. no end node            => "end" is larger than all keys   => return [begin node, max node)
        // 2. begin node == end node => such node is out of query range => return null range
        if (endNode == null)
            return new Pair<Node<V>, Node<V>>(beginNode, getMaxNode(root));
        else if (beginNode == endNode)
            return null;

        Node<V> lastNode = getPredecessor(endNode);
        assert lastNode != null;

        return new Pair<Node<V>, Node<V>>(beginNode, lastNode);
    }

    public boolean insert(Node<V> node)
    {
        K       nodeKey = conv.apply(node.value);
        Node<V> parent  = root;

        if (parent != null)
        {
            do
            {
                K   parentKey = getKey(parent);
                int cmp       = comp.compare(nodeKey, parentKey);

                if (cmp == 0)
                {
                    // refuse duplicated keys
                    return false;
                }
                else if (cmp < 0)
                {
                    Node<V> nextParent = getLeft(parent);

                    if (nextParent == null)
                    {
                        setLeft(parent, node);
                        break;
                    }

                    parent = nextParent;
                }
                else
                {
                    Node<V> nextParent = getRight(parent);

                    if (nextParent == null)
                    {
                        setRight(parent, node);
                        break;
                    }

                    parent = nextParent;
                }
            } while (true);
        }

        if (parent == null)
            root = node;

        setKey(node, nodeKey);
        setRed(node);
        setParent(node, parent);
        setLeft(node, null);
        setRight(node, null);

        ++count;

        return true;
    }

    public void insertCancel(Node<V> node)
    {
        assert isRed(node);
        assert !hasLeft(node);
        assert !hasRight(node);

        if (node == root)
        {
            root = null;
        }
        else
        {
            Node<V> parent = getParent(node);
            if (getLeft(parent) == node)
                setLeft(parent, null);
            else
                setRight(parent, null);

            setKey(node, null);
            setColor(node, null);
            setParent(node, null);
        }

        --count;
    }

    public void insertFixUp(Node<V> node)
    {
        assert isRed(node); // new node should be red

        if (node != root && isRed(getParent(node)))
        {
            if (getSibling(getParent(node)) != null && isRed(getSibling(getParent(node))))
            {
                setBlack(getParent(node));
                setBlack(getSibling(getParent(node)));
                setRed(getGrandParent(node));

                insertFixUp(node);
            }
            else if (getParent(node) == getLeft(getGrandParent(node)))
            {
                if (node == getRight(getParent(node)))
                    rotateLeft(node = getParent(node));

                setBlack(getParent(node));
                setRed(getGrandParent(node));
                rotateRight(getGrandParent(node));
            }
            else if (getParent(node) == getRight(getGrandParent(node)))
            {
                if (node == getLeft(getParent(node)))
                    rotateRight(node = getParent(node));

                setBlack(getParent(node));
                setRed(getGrandParent(node));
                rotateLeft(getGrandParent(node));
            }
        }

        setBlack(root);
    }

    public void delete(Node<V> node)
    {
        // we can't delete node with two children, exchange it and its successor
        // which has one or zero child
        if (hasLeft(node) && hasRight(node))
        {
            Node<V> replacer = getLeft(node);

            for (Node<V> replacerRight = getRight(replacer); replacerRight != null; replacerRight = getRight(replacer))
                replacer = replacerRight;

            if (replacer != node)
                exchangeNodes(node, replacer);
        }

        assert !hasLeft(node) || !hasLeft(node);

        Node<V> parent = getParent(node);
        Node<V> left   = getLeft(node);
        Node<V> right  = getRight(node);
        Node<V> leaf   = left != null ? left : right;

        if (leaf != null)
        {
            setParent(leaf, parent);

            if (parent == null)
                root = leaf;
            else if (getLeft(parent) == node)
                setLeft(parent, leaf);
            else
                setRight(parent, leaf);

            if (isBlack(node))
                deleteFixUp(leaf);
        }
        else if (parent == null)
        {
            // no leafs and parent => become empty tree

            root = null;
        }
        else
        {
            if (isBlack(node))
                deleteFixUp(node);

            if (getLeft(parent) == node)
                setLeft(parent, null);
            else
                setRight(parent, null);
        }

        --count;

        setKey(node, null);
        setColor(node, null);
        setParent(node, null);
        setLeft(node, null);
        setRight(node, null);
    }

    private void deleteFixUp(Node<V> node)
    {
        // TODO implement it

        while (node != root && isBlack(node))
        {
            if (node == getLeft(getParent(node)))
            {
                Node<V> sibling = getRight(getParent(node));

                if (isRed(sibling))
                {
                    setBlack(sibling);
                    setRed(getParent(node));

                    rotateLeft(getParent(node));

                    sibling = getRight(getParent(node));
                }

                if (isNilOrBlack(getLeft(sibling)) && isNilOrBlack(getRight(sibling)))
                {
                    setRed(sibling);

                    node = getParent(node);
                }
                else
                {
                    if (isNilOrBlack(getRight(sibling)))
                    {
                        setBlack(getLeft(sibling));
                        setRed(sibling);

                        rotateRight(sibling);

                        sibling = getRight(getParent(node));
                    }

                    setColor(sibling, getColor(getParent(node)));
                    setBlack(getParent(node));
                    setBlack(getRight(sibling));

                    rotateLeft(getParent(node));

                    node = root;
                }
            }
            else
            {
                Node<V> sibling = getLeft(getParent(node));

                if (isRed(sibling))
                {
                    setBlack(sibling);
                    setRed(getParent(node));

                    rotateRight(getParent(node));

                    sibling = getLeft(getParent(node));
                }

                if (isNilOrBlack(getLeft(sibling)) && isNilOrBlack(getRight(sibling)))
                {
                    setRed(sibling);

                    node = getParent(node);
                }
                else
                {
                    if (isNilOrBlack(getLeft(sibling)))
                    {
                        setBlack(getRight(sibling));
                        setRed(sibling);

                        rotateLeft(sibling);

                        sibling = getLeft(getParent(node));
                    }

                    setColor(sibling, getColor(getParent(node)));
                    setBlack(getParent(node));
                    setBlack(getLeft(sibling));

                    rotateRight(getParent(node));

                    node = root;
                }
            }
        }

        setBlack(node);
    }

    private void rotateLeft(Node<V> node)
    {
        //  parent         parent
        //    |              |
        //   node            c
        //   /  \           / \
        //  a    c   =>  node  d
        //      / \      /  \
        //     b   d    a    b

        Node<V> parent = getParent(node); // may be null
        Node<V> c      = getRight(node);
        Node<V> b      = getLeft(c);

        setParent(node, c);
        setRight(node, b);

        if (b != null)
            setParent(b, node);

        setParent(c, parent);
        setLeft(c, node);

        if (parent == null)
            root = c;
        else if (getLeft(parent) == node)
            setLeft(parent, c);
        else
            setRight(parent, c);
    }

    private void rotateRight(Node<V> node)
    {
        //    parent    parent
        //      |         |
        //     node       b
        //     /  \      / \
        //    b    d => a  node
        //   / \           /  \
        //  a   c         c    d

        Node<V> parent = getParent(node); // may be null
        Node<V> b      = getLeft(node);
        Node<V> c      = getRight(b);

        setParent(node, b);
        setLeft(node, c);

        setParent(b, parent);
        setRight(b, node);

        if (c != null)
            setParent(c, node);

        if (parent == null)
            root = b;
        else if (getLeft(parent) == node)
            setLeft(parent, b);
        else
            setRight(parent, b);
    }

    // exchange nodes only, color is kept the same in hierarchy
    private void exchangeNodes(Node<V> a, Node<V> b)
    {
        Integer aColor = getColor(a);
        Integer bColor = getColor(b);

        setColor(a, bColor);
        setColor(b, aColor);

        if (a == getParent(b))
            exchangeNodesIsParentAndChild(a, b);
        else if (b == getParent(a))
            exchangeNodesIsParentAndChild(b, a);
        else
            exchangeNodesIsNotParentAndChild(a, b);
    }

    private void exchangeNodesIsParentAndChild(Node<V> parent, Node<V> child)
    {
        assert getParent(child) == parent;

        Node<V> parentParent = getParent(parent);
        Node<V> parentLeft   = getLeft(parent);
        Node<V> parentRight  = getRight(parent);

        Node<V> childLeft  = getLeft(child);
        Node<V> childRight = getRight(child);

        setParent(parent, child);
        if (parentLeft == child)
        {
            setLeft(child, parent);

            setParent(parentRight, child);
            setRight(child, parentRight);
        }
        else
        {
            setRight(child, parent);

            setParent(parentLeft, parent);
            setLeft(child, parentLeft);
        }

        setLeft(parent, childLeft);
        if (childLeft != null)
            setParent(childLeft, parent);

        setRight(parent, childRight);
        if (childRight != null)
            setParent(childRight, parent);

        setParent(child, parentParent);
        if (parentParent == null)
            root = child;
        else if (getLeft(parentParent) == parent)
            setLeft(parentParent, child);
        else
            setRight(parentParent, child);
    }

    private void exchangeNodesIsNotParentAndChild(Node<V> a, Node<V> b)
    {
        Node<V> aParent = getParent(a);
        Node<V> aLeft   = getLeft(a);
        Node<V> aRight  = getRight(a);

        Node<V> bParent = getParent(b);
        Node<V> bLeft   = getLeft(b);
        Node<V> bRight  = getRight(b);

        assert bParent == null || aParent == null; // "a" or "b" or none of "a" and "b" will be root

        setParent(a, bParent);
        if (bParent == null)
            root = a;
        else if (getLeft(bParent) == b)
            setLeft(bParent, a);
        else
            setRight(bParent, a);

        setLeft(a, bLeft);
        if (bLeft != null)
            setParent(bLeft, a);

        setRight(a, bRight);
        if (bRight != null)
            setParent(bRight, a);

        setParent(b, aParent);
        if (aParent == null)
            root = b;
        else if (getLeft(aParent) == a)
            setLeft(aParent, b);
        else
            setRight(aParent, b);

        setLeft(b, aLeft);
        if (aLeft != null)
            setParent(aLeft, b);

        setRight(b, aRight);
        if (aRight != null)
            setParent(aRight, b);
    }

    Function<V, K> getConv()
    {
        return conv;
    }

    Comparator<K> getComp()
    {
        return comp;
    }

    Node<V> getRoot()
    {
        return root;
    }

    void setRoot(Node<V> root)
    {
        this.root = root;
    }

    @SuppressWarnings("unchecked")
    K getKey(Node<V> node)
    {
        return (K) node.payloads[getIdxKey()];
    }

    void setKey(Node<V> node, K key)
    {
        node.payloads[getIdxKey()] = key;
    }

    Node<V> getSibling(Node<V> node)
    {
        assert hasParent(node);

        Node<V> parent     = getParent(node);
        Node<V> parentLeft = getLeft(parent);

        if (parentLeft == null)
        {
            assert getRight(parent) == node;

            return null;
        }
        else if (parentLeft == node)
        {
            return getRight(parent);
        }
        else
        {
            return parentLeft;
        }
    }

    Node<V> getGrandParent(Node<V> node)
    {
        assert hasParent(node);
        assert hasParent(getParent(node));

        return getParent(getParent(node));
    }

    boolean hasParent(Node<V> node)
    {
        return getParent(node) != null;
    }

    @SuppressWarnings("unchecked")
    Node<V> getParent(Node<V> node)
    {
        return (Node<V>) node.payloads[getIdxParent()];
    }

    void setParent(Node<V> node, Node<V> parent)
    {
        node.payloads[getIdxParent()] = parent;
    }

    boolean hasLeft(Node<V> node)
    {
        return getLeft(node) != null;
    }

    @SuppressWarnings("unchecked")
    Node<V> getLeft(Node<V> node)
    {
        return (Node<V>) node.payloads[getIdxLeft()];
    }

    void setLeft(Node<V> node, Node<V> left)
    {
        node.payloads[getIdxLeft()] = left;
    }

    boolean hasRight(Node<V> node)
    {
        return getRight(node) != null;
    }

    @SuppressWarnings("unchecked")
    Node<V> getRight(Node<V> node)
    {
        return (Node<V>) node.payloads[getIdxRight()];
    }

    void setRight(Node<V> node, Node<V> right)
    {
        node.payloads[getIdxRight()] = right;
    }

    Integer getColor(Node<V> node)
    {
        return (Integer) node.payloads[getIdxColor()];
    }

    void setColor(Node<V> node, Integer color)
    {
        node.payloads[getIdxColor()] = color;
    }

    boolean isRed(Node<V> node)
    {
        return getColor(node) == RED;
    }

    void setRed(Node<V> node)
    {
        setColor(node, RED);
    }

    boolean isBlack(Node<V> node)
    {
        return getColor(node) == BLACK;
    }

    void setBlack(Node<V> node)
    {
        setColor(node, BLACK);
    }

    boolean isNilOrBlack(Node<V> node)
    {
        return node == null || isBlack(node);
    }

    Node<V> getPredecessor(Node<V> node)
    {
        assert node != null;

        Node<V> left = getLeft(node);
        if (left != null)
            return getMaxNode(left);

        while (true)
        {
            Node<V> parent = getParent(node);
            if (parent == null)
                return null;

            if (getRight(parent) == node)
                return parent;

            Node<V> parentLeft = getLeft(parent);
            if (parentLeft != null && parentLeft != node)
                return getMaxNode(parentLeft);

            node = parent;
        }
    }

    Node<V> getSuccessor(Node<V> node)
    {
        assert node != null;

        Node<V> right = getRight(node);
        if (right != null)
            return getMinNode(right);

        while (true)
        {
            Node<V> parent = getParent(node);
            if (parent == null)
                return null;

            if (getLeft(parent) == node)
                return parent;

            Node<V> parentRight = getRight(parent);
            if (parentRight != null && parentRight != node)
                return getMinNode(parentRight);

            node = parent;
        }
    }

    Node<V> getMinNode(Node<V> node)
    {
        assert node != null;

        while (true)
        {
            Node<V> left = getLeft(node);
            if (left == null)
                return node;

            node = left;
        }
    }

    Node<V> getMaxNode(Node<V> node)
    {
        assert node != null;

        while (true)
        {
            Node<V> right = getRight(node);
            if (right == null)
                return node;

            node = right;
        }
    }
}
