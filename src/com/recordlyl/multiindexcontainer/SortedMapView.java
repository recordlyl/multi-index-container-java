package com.recordlyl.multiindexcontainer;

import java.util.Iterator;
import java.util.Map;

public interface SortedMapView<K, V> extends Iterable<Map.Entry<K, V>>
{
    int size();
    boolean add(V e);
    boolean remove(K key);
    boolean remove(K begin, K end);

    Iterator<K> keyIterator();
    Iterator<V> valueIterator();
}
