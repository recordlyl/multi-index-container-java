package com.recordlyl.multiindexcontainer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;

public class MultiIndexContainer<E> implements SequencedView<E>
{
    private MainView<E>      bridge;
    private List<SubView<E>> subViews;
    private Sequenced<E>     seqView;
    private int              payloadCount;

    public MultiIndexContainer()
    {
        bridge = new MainView<E>()
        {
            @Override
            public boolean add(E element)
            {
                return MultiIndexContainer.this.internalAdd(element);
            }

            @Override
            public boolean remove(Node<E> node)
            {
                return MultiIndexContainer.this.internalRemove(node);
            }
        };

        seqView  = new Sequenced<E>(bridge, 0);
        subViews = new ArrayList<SubView<E>>();
        subViews.add(seqView);

        payloadCount = subViews.get(0).getPayloadCount();
    }

    public <K extends Comparable<K>> SortedMapView<K, E> sortedMapView(Function<E, K> conv)
    {
        return sortedMapView(conv, Ordering.<K>natural());
    }

    public <K> SortedMapView<K, E> sortedMapView(Function<E, K> conv, Comparator<K> comp)
    {
        if (seqView.size() != 0)
            throw new UnsupportedOperationException("cannot create view on non-empty container");

        SortedMap<K, E> view = new SortedMap<K, E>(bridge, conv, comp, payloadCount);

        subViews.add(view);
        payloadCount += view.getPayloadCount();

        return view;
    }

    @Override
    public int size()
    {
        return seqView.size();
    }

    @Override
    public boolean add(E element)
    {
        return seqView.add(element);
    }

    @Override
    public boolean remove(E e)
    {
        return seqView.remove(e);
    }

    @Override
    public Iterator<E> iterator()
    {
        return seqView.iterator();
    }

    private boolean internalAdd(E element)
    {
        Node<E> node = new Node<E>(element, payloadCount);

        {
            int i = -1;

            try
            {
                for (i = 0; i != subViews.size(); ++i)
                {
                    SubView<E> subView = subViews.get(i);

                    if (!subView.reserveAddNode(node))
                    {
                        while (i-- != 0)
                        {
                            SubView<E> subViewToRevert = subViews.get(i);

                            subViewToRevert.revertAddNode(node);
                        }

                        return false;
                    }
                }
            }
            catch (RuntimeException e)
            {
                while (i-- != 0)
                {
                    SubView<E> subView = subViews.get(i);

                    subView.revertAddNode(node);
                }

                throw e;
            }
        }

        for (int i = 0; i != subViews.size(); ++i)
        {
            SubView<E> subView = subViews.get(i);

            subView.commitAddNode(node);
        }

        return true;
    }

    private boolean internalRemove(Node<E> node)
    {
        for (SubView<E> subView : subViews)
            subView.removeNode(node);

        return true;
    }
}
