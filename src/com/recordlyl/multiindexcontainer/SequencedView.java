package com.recordlyl.multiindexcontainer;

public interface SequencedView<E> extends Iterable<E>
{
    int size();
    boolean add(E e);
    boolean remove(E e);
}
