package com.recordlyl.multiindexcontainer;

public class Pair<T, U>
{
    T first;
    U second;

    Pair()
    {
        this(null, null);
    }

    Pair(T first, U second)
    {
        this.first  = first ;
        this.second = second;
    }
}
