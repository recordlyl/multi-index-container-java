package com.recordlyl.multiindexcontainer;

interface MainView<E>
{
    boolean add(E element);
    boolean remove(Node<E> node);
}
