package com.recordlyl.multiindexcontainer;

class Node<E>
{
    E        value;
    Object[] payloads;

    Node(int payloadCount)
    {
        this(null, payloadCount);
    }

    Node(E value, int payloadCount)
    {
        this.value    = value;
        this.payloads = new Object[payloadCount];
    }
}
